//
//  User.swift
//  sharedspace
//
//  Created by John Murillo on 25/08/21.
//

import UIKit
import SwiftyJSON

class UserRequest: NSObject, NSCoding {

    var name:String!
    var email:String!
    var recoveryDate:String!
    var userName:String!
    var center:String!
    
    override init() {
        super.init()
    }
    
    init(json: JSON){
        self.name = json["name"].string
        self.email = json["email"].string
        self.recoveryDate = json["recoveryDate"].string
        self.userName = json["username"].string
        self.center = json["center"].string
    }
    
    func toCreateDictionary() -> [String:Any] {
        var dictionary = [String: Any]()
        dictionary["name"] = name
        dictionary["email"] = email
        dictionary["recoveryDate"] = recoveryDate
        dictionary["username"] = userName
        dictionary["center"] = center
        return dictionary
    }
    
    // MARK: NSCoding
    
    required init(coder: NSCoder) {
        self.name = coder.decodeObject(forKey: "name") as? String
        self.email = coder.decodeObject(forKey: "email") as? String
        self.recoveryDate = coder.decodeObject(forKey: "recoveryDate") as? String
        self.userName = coder.decodeObject(forKey: "username") as? String
        self.center = coder.decodeObject(forKey: "center") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.name, forKey: "name")
        coder.encode(self.email, forKey: "email")
        coder.encode(self.recoveryDate, forKey: "recoveryDate")
        coder.encode(self.userName, forKey: "username")
        coder.encode(self.center, forKey: "center")
    }
}

//
//  UserModels.swift
//  sharedspace
//
//  Created by John Murillo on 25/08/21.
//

import UIKit

enum UserModel {
    
    enum Creation {
        
        struct FirebaseUser {
            var email: String!
            var password: String!
        }
        
        struct Request {
            var name: String!
            var email: String!
            var recoveryDate: String!
            var username: String!
            var center: String!
        }
        
        struct Response {
            var clientId:String!
            var center:String!
            var accepted:Bool!
            var rejected:Bool!
            var pendient:Bool!
        }
    }
}

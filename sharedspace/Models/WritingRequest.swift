//
//  WritingRequest.swift
//  sharedspace
//
//  Created by John Murillo on 2/09/21.
//

import UIKit
import SwiftyJSON

class WritingRequest: NSObject, NSCoding {

    var open:String!
    var clientId:String!
    var title:String!
    var content:String!
    
    override init() {
        super.init()
    }
    
    init(json: JSON){
        self.open = json["open"].string
        self.clientId = json["clientId"].string
        self.title = json["title"].string
        self.content = json["content"].string
    }
    
    func toCreateDictionary() -> [String:Any] {
        var dictionary = [String: Any]()
        dictionary["open"] = open
        dictionary["clientId"] = clientId
        dictionary["title"] = title
        dictionary["content"] = content
        return dictionary
    }
    
    // MARK: NSCoding
    
    required init(coder: NSCoder) {
        self.open = coder.decodeObject(forKey: "open") as? String
        self.clientId = coder.decodeObject(forKey: "clientId") as? String
        self.title = coder.decodeObject(forKey: "title") as? String
        self.content = coder.decodeObject(forKey: "content") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.open, forKey: "open")
        coder.encode(self.clientId, forKey: "clientId")
        coder.encode(self.title, forKey: "title")
        coder.encode(self.content, forKey: "content")
    }
}


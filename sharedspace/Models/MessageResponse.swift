//
//  MessageResponse.swift
//  sharedspace
//
//  Created by John Murillo on 2/09/21.
//

import UIKit
import SwiftyJSON

class MessageResponse: NSObject, NSCoding {

    var message:String!
    
    override init() {
        super.init()
    }
    
    init(json: JSON){
        self.message = json["message"].string
    }
    
    func toCreateDictionary() -> [String:Any] {
        var dictionary = [String: Any]()
        dictionary["message"] = message
        return dictionary
    }
    
    // MARK: NSCoding
    
    required init(coder: NSCoder) {
        self.message = coder.decodeObject(forKey: "message") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.message, forKey: "message")
    }
}

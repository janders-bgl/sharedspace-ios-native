//
//  UserResponse.swift
//  sharedspace
//
//  Created by John Murillo on 25/08/21.
//

import UIKit
import SwiftyJSON

class UserResponse: NSObject, NSCoding {

    var id:String!
    var clientId:String!
    var center:String!
    var accepted:Bool!
    var rejected:Bool!
    var pendient:Bool!
    
    override init() {
        super.init()
    }
    
    init(json: JSON){
        self.id = json["id"].string
        self.clientId = json["clientId"].string
        self.center = json["center"].string
        self.accepted = json["accepted"].bool
        self.rejected = json["rejected"].bool
        self.pendient = json["pendient"].bool
    }
    
    func toCreateDictionary() -> [String:Any] {
        var dictionary = [String: Any]()
        dictionary["id"] = id
        dictionary["clientId"] = clientId
        dictionary["center"] = center
        dictionary["accepted"] = accepted
        dictionary["rejected"] = rejected
        dictionary["pendient"] = pendient
        return dictionary
    }
    
    // MARK: NSCoding
    
    required init(coder: NSCoder) {
        self.id = coder.decodeObject(forKey: "id") as? String
        self.clientId = coder.decodeObject(forKey: "clientId") as? String
        self.center = coder.decodeObject(forKey: "center") as? String
        self.accepted = coder.decodeObject(forKey: "accepted") as? Bool
        self.rejected = coder.decodeObject(forKey: "rejected") as? Bool
        self.pendient = coder.decodeObject(forKey: "pendient") as? Bool
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "id")
        coder.encode(self.clientId, forKey: "clientId")
        coder.encode(self.center, forKey: "center")
        coder.encode(self.accepted, forKey: "accepted")
        coder.encode(self.rejected, forKey: "rejected")
        coder.encode(self.pendient, forKey: "pendient")
    }

    func saveUser() {
        let defaults = UserDefaults.standard
        let savedData = try! NSKeyedArchiver.archivedData(withRootObject:self, requiringSecureCoding: false)
        defaults.set(savedData, forKey: Constants.USER)
    }
    
    static func loadUser() -> UserResponse? {
        let defaults = UserDefaults.standard
        let encodeObject = defaults.object(forKey: Constants.USER) as? Foundation.Data
        guard encodeObject != nil else{
            return nil
        }
        if let object = try? NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(encodeObject!) as? UserResponse {
            return object
        }
        return nil
    }
    
    static func removeUser() {
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: Constants.USER)
        defaults.synchronize()
    }
    
    static func isSessionActive() -> Bool {
        return UserResponse.loadUser() != nil
    }
}


//
//  CenterResponse.swift
//  sharedspace
//
//  Created by John Murillo on 26/08/21.
//

import UIKit
import SwiftyJSON

class CenterResponse: NSObject, NSCoding {

    var id:String!
    var name:String!
    
    override init() {
        super.init()
    }
    
    init(json: JSON){
        self.id = json["id"].string
        self.name = json["name"].string
    }
    
    func toCreateDictionary() -> [String:Any] {
        var dictionary = [String: Any]()
        dictionary["id"] = id
        dictionary["name"] = name
        return dictionary
    }
    
    // MARK: NSCoding
    
    required init(coder: NSCoder) {
        self.id = coder.decodeObject(forKey: "id") as? String
        self.name = coder.decodeObject(forKey: "name") as? String
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(self.id, forKey: "id")
        coder.encode(self.name, forKey: "name")
    }
}

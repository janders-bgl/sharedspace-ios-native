//
//  APIFeed.swift
//  sharedspace
//
//  Created by John Murillo on 2/09/21.
//

import Foundation
import FirebaseAuth

class APIFeed : SSClient {
    func postWriting(_ request: APIEndpoint, withCompletion handler: @escaping (_ success:Bool, _ message: MessageResponse?, _ error: Error?) -> Void)  {
        self.request(request) { (statusCode, response, error) in
            if error != nil {
                handler(false, nil, error)
            } else{
                let message = MessageResponse(json: response!)
                handler(true, message, nil)
            }
        }
    }
}

extension APIFeed {
    static func postWritingRequest(writing : WritingRequest) -> APIEndpoint {
        let urlPath: String = Constants.API_CREATE_FEED_WRITING
        let tokenString = Constants.TOKEN
        return APIEndpoint(
            method: .post,
            path: urlPath,
            parameters: writing.toCreateDictionary(),
            headers:["Authorization": tokenString]
        )
    }
}

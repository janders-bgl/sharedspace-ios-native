//
//  APIEndpoint.swift
//  sharedspace
//
//  Created by John Murillo on 25/08/21.
//

import Foundation

typealias Parameters = [String: Any]
typealias Headers = [String: String]
typealias Path = String

enum Method {
    case get, post, put, patch, delete
}

// MARK: Endpoint
public final class APIEndpoint: NSObject {
    let method: Method
    let path: Path
    let parameters: Parameters?
    var headers: Headers?

    init(method: Method = .get,
         path: Path,
         parameters: Parameters? = nil,
         headers: Headers? = [:]) {
        self.method = method
        self.path = path
        self.parameters = parameters
        self.headers = headers
    }
}

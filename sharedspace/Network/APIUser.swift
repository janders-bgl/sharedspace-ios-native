//
//  APIUser.swift
//  sharedspace
//
//  Created by John Murillo on 25/08/21.
//

import Foundation
import FirebaseAuth

class APIUser : SSClient {
    func createFirebaseUser(_ email: String, _ password:String , withCompletion handler: @escaping (_ success:Bool, _ error: Error?) -> Void)  {
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            if authResult != nil {
                handler(true, nil)
            } else {
                handler(false, error)
            }
        }
    }
    
    func createSSUser(_ request: APIEndpoint, withCompletion handler: @escaping (_ success:Bool, _ user: UserResponse?, _ error: Error?) -> Void)  {
        self.request(request) { (statusCode, response, error) in
            if error != nil {
                handler(false, nil, error)
            } else{
                let user = UserResponse(json: response!)
                handler(true, user, nil)
            }
        }
    }
    
    func logIn(_ email: String, _ password:String , withCompletion handler: @escaping (_ success:Bool, _ id: String?, _ error: Error?) -> Void)  {
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            if let userObj = authResult {
                let user = userObj.user
                handler(true, user.uid, error)
            } else {
                handler(false, nil, error)
            }
        }
    }
    
    func getCenters(_ request: APIEndpoint, withCompletion handler: @escaping (_ success:Bool, _ centerList: [CenterResponse]?, _ error: Error?) -> Void)  {
        self.request(request) { (statusCode, response, error) in
            if error != nil {
                handler(false, nil, error)
            } else {
                var centerList : [CenterResponse] = []
                let centers = response?.array ?? []
                for center in centers {
                    centerList.append(CenterResponse(json: center))
                }
                handler(true, centerList, nil)
            }
        }
    }
    
    func getUserDetails(_ request: APIEndpoint, withCompletion handler: @escaping (_ success:Bool, _ user: UserResponse?, _ error: Error?) -> Void)  {
        self.request(request) { (statusCode, response, error) in
            if error != nil {
                handler(false, nil, error)
            } else {
                let user = UserResponse(json: response!)
                handler(true, user, nil)
            }
        }
    }
}

extension APIUser {
    static func createUserRequest(user : UserRequest) -> APIEndpoint {
        let urlPath: String = Constants.API_CREATE_USER
        return APIEndpoint(
            method: .post,
            path: urlPath,
            parameters: user.toCreateDictionary()
        )
    }
    
    static func getCentersRequest() -> APIEndpoint {
        let urlPath: String = Constants.API_GET_CENTERS
        return APIEndpoint(
            method: .get,
            path: urlPath
        )
    }
    
    static func getUserDetailsRequest(clientId: String) -> APIEndpoint {
        let urlPath: String = Constants.API_GET_USER_DETAILS + clientId
        let tokenString = Constants.TOKEN
        return APIEndpoint(
            method: .get,
            path: urlPath,
            headers:["Authorization": tokenString]
        )
    }
}

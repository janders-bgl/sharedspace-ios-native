//
//  SSClient.swift
//  sharedspace
//
//  Created by John Murillo on 25/08/21.
//

import Foundation
import Alamofire
import SwiftyJSON

class SSClient: NSObject {
    
    private static let manager: SessionManager! = {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 300 // seconds
        configuration.timeoutIntervalForResource = 300
        let alamoFireManager = Alamofire.SessionManager(configuration: configuration)
        return alamoFireManager
    }()
    
    private func url(path: Path) -> URL {
        return URL(string: "\(path)"
            .addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) ?? "")!
    }
    
    func request(_ endpoint: APIEndpoint,
                 withCompletion handler: @escaping (
        _ statusCode: Int?,
        _ response: JSON?,
        _ error: Error?) -> Void) {
        SSClient.manager.request(self.url(path: endpoint.path),
                             method: httpMethod(from: endpoint.method),
                             parameters: endpoint.parameters, encoding: JSONEncoding.default, headers: endpoint.headers)
            .responseData { (response) in
                
            switch response.result {
            case .success:
                if let statusCode = response.response?.statusCode {
                    if statusCode >= 400 {
                        if let value = response.result.value {
                            let json = JSON(value)
                            let message = json["message"].string
                            let code = json["errorCode"].string
                            let userInfo:[String:Any] =  [NSLocalizedDescriptionKey: message ?? "Unexpected error"]
                            let errorTemp = NSError(domain: code ?? "",
                                                    code: statusCode,
                                                    userInfo: userInfo)
                            handler(nil, nil, errorTemp)
                        }
                    } else {
                        var json: JSON?
                        if let value = response.result.value {
                            json = JSON(value)
                        }
                        handler(response.response?.statusCode, json, nil)
                    }
                }
            case .failure(let error):
                handler(nil, nil, error)
            }
        }
    }
}

private func httpMethod(from method: Method) -> Alamofire.HTTPMethod {
    switch method {
    case .get: return .get
    case .post: return .post
    case .put: return .put
    case .patch: return .patch
    case .delete: return .delete
    }
}

//
//  WriteViewController.swift
//  sharedspace
//
//  Created by John Murillo on 1/09/21.
//

import UIKit

class WriteViewController: UIViewController {

    @IBOutlet weak var counter: UILabel!
    @IBOutlet weak var userWriting: UITextView!
    
    var timer = Timer()
    var seconds = 0
    var minutes = 5
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapToQuitKeyboard = UITapGestureRecognizer(target: self, action: #selector(quitKeyboard))
        view.addGestureRecognizer(tapToQuitKeyboard)
        
        userWriting.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
        startCountdown()
    }
    
    @IBAction func discardAction (sender: Any){
        let alert = UIAlertController(title: "Are you sure?", message: "You are just going to discard this feed completely", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "NO", style: .cancel))
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action: UIAlertAction!) in
            self.performSegue(withIdentifier: "discardHomeSegue", sender: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func continueAction (sender: Any){
        if(!userWriting.text.isEmpty){
            self.performSegue(withIdentifier: "titleWritingSegue", sender: nil)
        } else {
            let alert = UIAlertController(title: "Empty writing", message: "Please, fill in the blank field, your writing is empty yet", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func startCountdown(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(counterDown), userInfo: nil, repeats: true)
    }
    
    @objc func counterDown(){
        if(seconds == 0){
            if(minutes == 0){
                stop()
            } else {
                seconds = 59
                minutes -= 1
            }
        } else {
            seconds -= 1
        }
        
        counter.text = String(minutes).appending(":").appending(String(format: "%02d", seconds))
    }
    
    func stop(){
        timer.invalidate()
        if(!userWriting.text.isEmpty){
            self.performSegue(withIdentifier: "titleWritingSegue", sender: nil)
        } else {
            self.performSegue(withIdentifier: "backHomeSegue", sender: nil)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "titleWritingSegue"){
            let titleWritingSegue = segue.destination as! TitleWritingViewController
            titleWritingSegue.streamOfConsciousness = userWriting.text
        }
    }
    
    @IBAction func quitKeyboard(){
        view.endEditing(true)
    }
}

//
//  TitleWritingViewController.swift
//  sharedspace
//
//  Created by John Murillo on 2/09/21.
//

import UIKit

class TitleWritingViewController: UIViewController {
    
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var sharingMode: UISegmentedControl!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var streamOfConsciousness : String!
    var open = false

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tapToQuitKeyboard = UITapGestureRecognizer(target: self, action: #selector(quitKeyboard))
        view.addGestureRecognizer(tapToQuitKeyboard)
        
        loadingIndicator.hidesWhenStopped = true
    }
    
    @IBAction func sharingModeChanged(_ sender: Any) {
        switch sharingMode.selectedSegmentIndex
            {
            case 0:
                open = true
            case 1:
                open = false
            default:
                break
            }
    }
    
    @IBAction func discardAction (sender: Any){
        let alert = UIAlertController(title: "Are you sure?", message: "You are just going to discard this feed completely", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "NO", style: .cancel))
        alert.addAction(UIAlertAction(title: "YES", style: .default, handler: { (action: UIAlertAction!) in
            self.performSegue(withIdentifier: "discardHomeSegue", sender: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    @IBAction func shareAction (sender: Any){
        if(!(titleTF.text?.isEmpty ?? false)){
            loadingIndicator.startAnimating()
            createFeedWriting(withCompletion: { (success, message, error) in
                self.loadingIndicator.stopAnimating()
                if (success) {
                    let alert = UIAlertController(title: "Post shared!", message: "Your writing was successfully shared", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action: UIAlertAction!) in
                        self.performSegue(withIdentifier: "discardHomeSegue", sender: nil)
                    }))
                    alert.addAction(UIAlertAction(title: "GO TO FEED", style: .default, handler: { (action: UIAlertAction!) in
                        // Go to Feed instead of Home
                        self.performSegue(withIdentifier: "discardHomeSegue", sender: nil)
                    }))
                    self.present(alert, animated: true, completion: nil)
                } else {
                    let alert = UIAlertController(title: "Post failed!", message: error?.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        } else {
            let alert = UIAlertController(title: "There is no title", message: "Please, fill in the title field, it is necessary to post your feed", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func quitKeyboard(){
        view.endEditing(true)
    }
    
    // This must be into a viewmodel class
    
    func createFeedWriting(withCompletion handler: @escaping (_ success:Bool, _ message: MessageResponse?, _ error: Error?) -> Void) {
        let client = APIFeed()
        let writingToPost = self.makeRequest()
        let endpoint = APIFeed.postWritingRequest(writing: writingToPost)
        client.postWriting(endpoint) { (success, message, error) in
            if success {
                handler(success, message, error)
            } else {
                handler(success, nil, error)
            }
        }
    }
    
    func makeRequest() -> WritingRequest {
        let writing = WritingRequest()
        writing.open = String(self.open)
        writing.clientId = UserResponse.loadUser()?.id
        writing.title = self.titleTF.text
        writing.content = self.streamOfConsciousness
        return writing
    }
}

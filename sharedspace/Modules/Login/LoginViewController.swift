//
//  LoginViewController.swift
//  sharedspace
//
//  Created by John Murillo on 4/08/21.
//

import UIKit

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var forgotPassword: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var register: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let validator = Validator()
    var formError = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        loadingIndicator.hidesWhenStopped = true
        let tapToQuitKeyboard = UITapGestureRecognizer(target: self, action: #selector(quitKeyboard))
        view.addGestureRecognizer(tapToQuitKeyboard)
    }
    
    @IBAction func loginAction (sender: Any){
        if (validateForm()){
            loadingIndicator.startAnimating()
            if let email = emailTF.text, let pw = passwordTF.text {
                logInFirebase(email, pw) { (success, id, error) in
                    if !success {
                        self.loadingIndicator.stopAnimating()
                        let alert = UIAlertController(title: "Failed login", message: error?.localizedDescription, preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(alert, animated: true, completion: nil)
                    } else {
                        self.getUserDetails(id, withCompletion: { (success, user, error) in
                            self.loadingIndicator.stopAnimating()
                            if (success) {
                                user?.saveUser()
                                if(user!.accepted){
                                    self.performSegue(withIdentifier: "homeSegue", sender: nil)
                                } else {
                                    let alert = UIAlertController(title: "Waiting for approval",
                                                                  message: "Your user is stil waiting for approval from your tratement center",
                                                                  preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            }
                        })
                    }
                }
            }
        } else {
            let alert = UIAlertController(title: "Wrong field", message: formError, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func validateForm() -> Bool {
        var isValid = false
        isValid = validator.validateRules([ValidationRule(text: emailTF.text ?? "", rules: [.isEmail], fieldName: "", condition: nil)]) == nil ? true : false
        if (isValid){
            if ((passwordTF.text ?? "").count > 7){
                return true
            } else {
                formError = "The password is not correct"
                return false
            }
        } else {
            formError = "The typed email has wrong format"
            return false
        }
    }
    
    @IBAction func quitKeyboard(){
        view.endEditing(true)
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardAppear(_:)), name:UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardDisappear(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func onKeyboardAppear(_ notification: NSNotification) {
        let info = notification.userInfo!
        let rect: CGRect = info[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
        let kbSize = rect.size

        let insets = UIEdgeInsets(top: 0, left: 0, bottom: kbSize.height, right: 0)
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets

        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect = self.view.frame;
        aRect.size.height -= kbSize.height;

        let activeField: UITextField? = [emailTF, passwordTF].first { $0.isFirstResponder }
        if let activeField = activeField {
            if !aRect.contains(activeField.frame.origin) {
                let scrollPoint = CGPoint(x: 0, y: activeField.frame.origin.y-kbSize.height)
                scrollView.setContentOffset(scrollPoint, animated: true)
            }
        }
    }

    @objc func onKeyboardDisappear(_ notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    // This must be into a viewmodel class
    
    func logInFirebase(_ email: String, _ password:String , withCompletion handler: @escaping (_ success:Bool, _ id: String?, _ error: Error?) -> Void)  {
        let client = APIUser()
        client.logIn(email, password) { (success, id, error) in
            handler(success, id, error)
        }
    }
    
    func getUserDetails(_ clientId: String!, withCompletion handler: @escaping (_ success:Bool, _ user: UserResponse?, _ error: Error?) -> Void)  {
        let client = APIUser()
        let endpoint = APIUser.getUserDetailsRequest(clientId: clientId)
        client.getUserDetails(endpoint) { (success, user, error) in
            if success {
                handler(success, user, error)
            } else {
                handler(success, nil, error)
            }
        }
    }
}

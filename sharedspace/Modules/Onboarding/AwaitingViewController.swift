//
//  AwaitingViewController.swift
//  sharedspace
//
//  Created by John Murillo on 23/08/21.
//

import UIKit

class AwaitingViewController: UIViewController {
    
    @IBOutlet weak var waitingForLabel: UILabel!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    var centerName : String!
    var clientId: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingIndicator.hidesWhenStopped = true
        waitingForLabel.text = "Waiting for ".appending(centerName).appending("...")
    }
    
    @IBAction func refreshAction(_ sender: Any) {
        loadingIndicator.startAnimating()
        getUserDetails(clientId, withCompletion: { (success, user, error) in
            self.loadingIndicator.stopAnimating()
            if (success) {
                if(user!.accepted){
                    self.performSegue(withIdentifier: "homeSegue", sender: nil)
                } else {
                    let alert = UIAlertController(title: "Waiting for approval",
                                                  message: "Your user is stil waiting for approval from ".appending(self.centerName),
                                                  preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(alert, animated: true, completion: nil)
                }
            }
        })
    }
    
    // This must be into a viewmodel class
    
    func getUserDetails(_ clientId: String!, withCompletion handler: @escaping (_ success:Bool, _ user: UserResponse?, _ error: Error?) -> Void)  {
        let client = APIUser()
        let endpoint = APIUser.getUserDetailsRequest(clientId: clientId)
        client.getUserDetails(endpoint) { (success, user, error) in
            if success {
                handler(success, user, error)
            } else {
                handler(success, nil, error)
            }
        }
    }
}

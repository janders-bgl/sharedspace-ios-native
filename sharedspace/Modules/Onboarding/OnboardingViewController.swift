//
//  OnboardingViewController.swift
//  sharedspace
//
//  Created by John Murillo on 22/08/21.
//

import UIKit

class OnboardingViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var centers : [CenterResponse] = []
    let screenWidth = UIScreen.main.bounds.width - 30
    let screenHeight = UIScreen.main.bounds.height / 2
    var selectedRow = 0
    var selectedId : String!
    var selectedName : String!
    
    @IBOutlet weak var yesButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCenters(withCompletion: { (success, user, error) in
            if (!success) {
                print("There is no information about the centers")
            }
        })
    }
    
    @IBAction func withACommunity(_ sender: Any) {
        let vc = UIViewController()
        vc.preferredContentSize = CGSize(width: screenWidth, height: screenHeight)
        
        let pickerView = UIPickerView(frame: CGRect(x: 0, y: 0, width: screenWidth, height: screenHeight))
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.selectRow(selectedRow, inComponent: 0, animated: false)
        
        vc.view.addSubview(pickerView)
        pickerView.centerXAnchor.constraint(equalTo: vc.view.centerXAnchor).isActive = true
        pickerView.centerYAnchor.constraint(equalTo: vc.view.centerYAnchor).isActive = true
        
        let alert = UIAlertController(title: "Select your organization", message: "", preferredStyle: .actionSheet)
        alert.popoverPresentationController?.sourceView = yesButton
        alert.popoverPresentationController?.sourceRect = yesButton.bounds
        
        alert.setValue(vc, forKey: "contentViewController")
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (UIAlertAction) in }))
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { (UIAlertAction) in
            self.selectedRow = pickerView.selectedRow(inComponent: 0)
            self.selectedId = self.centers[self.selectedRow].id
            self.selectedName = self.centers[self.selectedRow].name
            self.performSegue(withIdentifier: "registerSegue", sender: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let label = UILabel(frame: CGRect(x: 0, y: 0, width: screenWidth, height: 30))
        label.text = centers[row].name
        label.sizeToFit()
        
        return label
    }
    
    func numberOfComponents (in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return centers.count
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 60
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return centers[row].name
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "registerSegue"){
            let registerSegue = segue.destination as! RegisterViewController
            registerSegue.centerId = selectedId
            registerSegue.centerName = selectedName
        }
    }
    
    // This must be into a viemodel class
    
    func getCenters( withCompletion handler: @escaping (_ success:Bool, _ centers: [CenterResponse]?, _ error: Error?) -> Void) {
        let client = APIUser()
        let endpoint = APIUser.getCentersRequest()
        client.getCenters(endpoint) { (success, centers, error) in
            if success {
                self.centers = centers ?? []
            }
            handler(success, centers, error)
        }
    }
}

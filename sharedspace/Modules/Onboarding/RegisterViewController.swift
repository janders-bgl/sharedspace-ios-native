//
//  RegisterViewController.swift
//  sharedspace
//
//  Created by John Murillo on 22/08/21.
//

import UIKit
import Alamofire

class RegisterViewController: UIViewController {
    
    @IBOutlet weak var fullNameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var passwordConfirmationTF: UITextField!
    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var recoveryDate: UITextField!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    let validator = Validator()
    var formError = ""
    var recoveryDateTimeStamp = Int64(0)
    var centerId : String!
    var centerName : String!
    var clientId : String? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadingIndicator.hidesWhenStopped = true
        
        let tapToQuitKeyboard = UITapGestureRecognizer(target: self, action: #selector(quitKeyboard))
        view.addGestureRecognizer(tapToQuitKeyboard)
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Calendar.current.date(byAdding: .day, value: 0, to: Date())
        datePicker.locale = Locale(identifier: "en_US_POSIX")
        datePicker.addTarget(self, action: #selector(handleDateSelection(sender:)), for: UIControl.Event.valueChanged)
        recoveryDate.inputView = datePicker
    }
    
    @IBAction func continueAction (sender: Any){
        if (validateForm()){
            loadingIndicator.startAnimating()
            var userRequest = UserModel.Creation.Request()
            userRequest.name = fullNameTF.text
            userRequest.email = emailTF.text
            userRequest.recoveryDate = String(recoveryDateTimeStamp)
            userRequest.username = usernameTF.text
            userRequest.center = centerId
            
            var fbUser = UserModel.Creation.FirebaseUser()
            fbUser.email = emailTF.text
            fbUser.password = passwordTF.text
            
            createUser(firebaseUser: fbUser, ssUser: userRequest, withCompletion: { (success, user, error) in
                self.loadingIndicator.stopAnimating()
                if (success) {
                    if(self.centerId == nil){
                        self.performSegue(withIdentifier: "homeSegue", sender: nil)
                    } else {
                        self.performSegue(withIdentifier: "awaitingSegue", sender: nil)
                    }
                } else {
                    let alert = UIAlertController(title: "Failed registration", message: error?.localizedDescription, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default))
                    self.present(alert, animated: true, completion: nil)
                }
            })
        } else {
            let alert = UIAlertController(title: "Wrong field", message: formError, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    private func validateForm() -> Bool {
        var isValid = false
        
        if((fullNameTF.text ?? "").count > 1){
            isValid = validator.validateRules([ValidationRule(text: emailTF.text ?? "", rules: [.isEmail], fieldName: "", condition: nil)]) == nil ? true : false
            if (isValid){
                isValid = validator.validateRules([ValidationRule(text: passwordTF.text ?? "", rules: [.minLenght], fieldName: "", condition: 8)]) == nil ? true : false
                if (isValid){
                    isValid = validator.validateRules([ValidationRule(text: passwordTF.text ?? "", rules: [.oneNumber], fieldName: "", condition: nil)]) == nil ? true : false
                    if(isValid){
                        isValid = validator.validateRules([ValidationRule(text: passwordTF.text ?? "", rules: [.oneUppercase], fieldName: "", condition: nil)]) == nil ? true : false
                        if(isValid){
                            isValid = validator.validateRules([ValidationRule(text: passwordTF.text ?? "", rules: [.oneLowercase], fieldName: "", condition: nil)]) == nil ? true : false
                            if(isValid){
                                isValid = validator.validateRules([ValidationRule(text: passwordTF.text ?? "", rules: [.equal], fieldName: "", condition: self.passwordConfirmationTF.text ?? "")]) == nil ? true : false
                                if(isValid){
                                    if((usernameTF.text ?? "").count > 1){
                                        if((recoveryDate.text ?? "").count > 0){
                                            return true
                                        } else {
                                            formError = "The \"Recovery date\" field must not be empty"
                                            return false
                                        }
                                    } else {
                                        formError = "The username field must not be empty"
                                        return false
                                    }
                                } else {
                                    formError = "The password typed on the field \"Confirmation password\" is not the same typed on \"New password\" field"
                                    return false
                                }
                            } else {
                                formError = "The password must have at least one lowercase letter"
                                return false
                            }
                        } else {
                            formError = "The password must have at least one uppercase letter"
                            return false
                        }
                    } else {
                        formError = "The password must have at least one number"
                        return false
                    }
                } else {
                    formError = "The password must have at least 8 characters"
                    return false
                }
            } else {
                formError = "The typed email has wrong format"
                return false
            }
        } else {
            formError = "It's missing a full name on the first field"
            return false
        }
    }
    
    @objc func handleDateSelection(sender: UIDatePicker){
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "d MMM, YYYY"
        
        recoveryDate.text = dateFormatter.string(from: sender.date)
        recoveryDateTimeStamp = Int64((sender.date.timeIntervalSince1970)*1000)
    }
    
    @IBAction func quitKeyboard(){
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "awaitingSegue"){
            let awaitingSegue = segue.destination as! AwaitingViewController
            awaitingSegue.centerName = centerName
            awaitingSegue.clientId = clientId
        } else if(segue.identifier == "homeSegue"){
            let homeSegue = segue.destination as! HomeViewController
        }
    }
    
    func registerForKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardAppear(_:)), name:UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onKeyboardDisappear(_:)), name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name:UIResponder.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardDidHideNotification, object: nil)
    }
    
    @objc func onKeyboardAppear(_ notification: NSNotification) {
        let info = notification.userInfo!
        let rect: CGRect = info[UIResponder.keyboardFrameBeginUserInfoKey] as! CGRect
        let kbSize = rect.size

        let insets = UIEdgeInsets(top: 0, left: 0, bottom: kbSize.height, right: 0)
        scrollView.contentInset = insets
        scrollView.scrollIndicatorInsets = insets

        // If active text field is hidden by keyboard, scroll it so it's visible
        // Your application might not need or want this behavior.
        var aRect = self.view.frame;
        aRect.size.height -= kbSize.height;

        let activeField: UITextField? = [emailTF, passwordTF].first { $0.isFirstResponder }
        if let activeField = activeField {
            if !aRect.contains(activeField.frame.origin) {
                let scrollPoint = CGPoint(x: 0, y: activeField.frame.origin.y-kbSize.height)
                scrollView.setContentOffset(scrollPoint, animated: true)
            }
        }
    }

    @objc func onKeyboardDisappear(_ notification: NSNotification) {
        scrollView.contentInset = UIEdgeInsets.zero
        scrollView.scrollIndicatorInsets = UIEdgeInsets.zero
    }
    
    // This must be into a viewmodel class
    
    func createUser(firebaseUser: UserModel.Creation.FirebaseUser, ssUser: UserModel.Creation.Request, withCompletion handler: @escaping (_ success:Bool, _ user: UserResponse?, _ error: Error?) -> Void) {
        let client = APIUser()
        client.createFirebaseUser(firebaseUser.email, firebaseUser.password) { (success, error) in
            if success {
                let userToCreate = self.userFromRequest(request: ssUser)
                let endpoint = APIUser.createUserRequest(user: userToCreate)
                client.createSSUser(endpoint) { (success, createdUser, error) in
                    if success {
                        self.clientId = createdUser?.clientId
                    }
                    handler(success, createdUser, error)
                }
            } else {
                handler(success, nil, error)
            }
        }
    }
    
    func userFromRequest(request: UserModel.Creation.Request) -> UserRequest {
        let user = UserRequest()
        user.name = request.name
        user.email = request.email
        user.recoveryDate = String(request.recoveryDate)
        user.userName = request.username
        user.center = request.center
        return user
    }
}

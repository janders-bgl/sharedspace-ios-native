//
//  HomeViewController.swift
//  sharedspace
//
//  Created by John Murillo on 24/08/21.
//

import UIKit

class HomeViewController: UIViewController {

    @IBOutlet weak var feedButton: UIButton!
    @IBOutlet weak var writeButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupBorderToButtons()
    }
    
    func setupBorderToButtons(){
        setupBorderedButton(button: feedButton)
        setupBorderedButton(button: writeButton)
        setupBorderedButton(button: profileButton)
        setupBorderedButton(button: recordButton)
    }
    
    func setupBorderedButton(button: UIButton){
        button.layer.cornerRadius = 24
        button.layer.borderWidth = 1.5
        button.layer.masksToBounds = false
        button.layer.borderColor = CGColor.init(red: 256, green: 256, blue: 256, alpha: 1)
        button.clipsToBounds = true
    }
}

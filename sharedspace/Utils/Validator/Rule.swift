//
//  Rule.swift
//  sharedspace
//
//  Created by John Murillo on 26/08/21.
//

import Foundation

struct Rule {
    
    let validate: (String, String, Any?) -> String?

    static let isEmail = Rule(validate: { _, text, _ in
        let regex = #"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,64}"#

        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: text) ? nil : (NSLocalizedString("sign_up_vc_email_error", comment: ""))
    })
    
    static let oneNumber = Rule(validate: { fieldName, text, _ in
        let regex = ".*[0-9]+.*"

        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: text) ? nil : "\(String(describing: fieldName)) \("")"
    })
    
    static let oneUppercase = Rule(validate: { fieldName, text, _ in
        let regex = ".*[A-Z]+.*"

        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: text) ? nil : "\(String(describing: fieldName)) \("")"
    })

    static let oneLowercase = Rule(validate: { fieldName, text, _ in
        let regex = ".*[a-z]+.*"

        let predicate = NSPredicate(format: "SELF MATCHES %@", regex)
        return predicate.evaluate(with: text) ? nil : "\(String(describing: fieldName)) \("")"
    })
    
    static let minLenght = Rule(validate: { fieldName, text, value in
        let num = value as? Int
        return text.count >= num! ? nil : "\(String(describing: fieldName)) \("")"
    })
    
    static let equal = Rule(validate: { fieldName, text, value in
        let compare = value as? String
        return text == compare ? nil :"\(String(describing: fieldName)) \("")"
    })
}

//
//  ValidationRule.swift
//  sharedspace
//
//  Created by John Murillo on 26/08/21.
//

import Foundation

struct ValidationRule {
    
    var text: String
    var rules: [Rule]
    var fieldName: String
    var condition: Any?
    
    init(text: String, rules: [Rule], fieldName: String, condition: Any?) {
        self.text = text
        self.rules = rules
        self.fieldName = fieldName
        self.condition = condition
    }
}

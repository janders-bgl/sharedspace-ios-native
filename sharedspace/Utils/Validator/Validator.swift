//
//  Validator.swift
//  sharedspace
//
//  Created by John Murillo on 26/08/21.
//

import Foundation

class Validator {
    
    func validate(_ rule: ValidationRule) -> String? {
        
        return rule.rules.compactMap({ $0.validate(rule.fieldName, rule.text, rule.condition) }).first
    }
    
    func validateRules(_ rules: [ValidationRule]) -> String? {
        
        var errorMessage: String?
        
        for myRule in rules {
            if let error = self.validate(myRule) {
                errorMessage = error
                break
            }
        }
        
        return errorMessage
        
    }
}

//
//  Constants.swift
//  sharedspace
//
//  Created by John Murillo on 25/08/21.
//

import Foundation

class Constants: NSObject {

    // MARK: Base URL Services
    static let BASE_URL_CLIENTS = "https://api-clients-emqqibvzxa-ue.a.run.app/api/v1"
    static let BASE_URL_CENTERS = "https://api-centers-emqqibvzxa-ue.a.run.app/api/v1"
    static let BASE_URL_FEEDS = "https://api-feed-emqqibvzxa-ue.a.run.app/api/v1"
    
    static let API_CREATE_USER = BASE_URL_CLIENTS.appending("/clients/new")
    static let API_GET_USER_DETAILS = BASE_URL_CLIENTS.appending("/clients/")
    
    static let API_GET_CENTERS = BASE_URL_CENTERS.appending("/centers")
    
    static let API_CREATE_FEED_WRITING = BASE_URL_FEEDS.appending("/feed/write")
    
    static let TOKEN = "Bearer ACzBnCgSPtrM-p1izE0usXe1W8Mgl3qTXXI7F9viZaE6g9BTZE_BZLhXINiWROuBZafjdrtJ7WRSnaNJvftMv7iZ0EKtj-JGed3BTMqTdrmSQowwB2TxJlDyeX1AxWMt2eEPZGKiQBcXQSWXLcYDMLx0Zz-SWTUVg2sT5LXhfVGFVOn001Hj1wYrgzvp1EdK6hIoOwIjNgD6Pf4bTAgeEn7sIDBQAvaQTYMhP6v5UdZcbt5wzkghk0wxG1WCyHebm20M2RIIyAV9"
    
    // MARK: Keys
    static let USER = "user"
}
